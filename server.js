'use strick';

import express from 'express';
import bodyParser from 'body-parser';
import consign from 'consign';
require('dotenv').config()

const app = express();

// Loading API modules
consign()
  .include('./app/v1')
  .then('./app/v2')
  .into(app);

const port = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.listen(port, ()=>{
  console.log('API server running on port: ' + port);
});
