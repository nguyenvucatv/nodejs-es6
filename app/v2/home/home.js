'use strick';

module.exports = app => {

  // Home
  app.get('/api/v2', (req, res) =>{
    res.json(200, { msg: 'Home v2'});
  });

  // About
  app.get('/api/v2/about', (req, res) =>{
    res.json(200, { msg: 'About v2'});
  });

  // Contact
  app.get('/api/v2/contact', (req, res) =>{
    res.json(200, { msg: 'Contact v2'});
  });

};
