'use strick';

module.exports = app => {

  // Home
  app.get('/api/v1', (req, res) =>{
    res.json(200, { msg: 'Home v1'});
  });

  // About
  app.get('/api/v1/about', (req, res) =>{
    res.json(200, { msg: 'About v1'});
  });

  // Contact
  app.get('/api/v1/contact', (req, res) =>{
    res.json(200, { msg: 'Contact v1'});
  });

};
